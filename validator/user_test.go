package validator

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
)

var (
	reqJSONCase1 = `{"name": "Taro", "age": 1}`
	reqJSONCase2 = `{"name": "", "age": 1}`
	reqJSONCase3 = `{"name": "Taro", "age": ""}`
	reqJSONCase4 = `{"aaa": "1"}`
)

func TestInsertUser(t *testing.T) {

	url := "http://localhost:8000/api/users/"

	err := testCase1(url, reqJSONCase1)
	if err != nil {
		t.Error(err)
		return
	}

	err = testCase2(url, reqJSONCase2)
	if err != nil {
		t.Error(err)
		return
	}

	err = testCase3(url, reqJSONCase3)
	if err != nil {
		t.Error(err)
		return
	}

	err = testCase4(url, reqJSONCase4)
	if err != nil {
		t.Error(err)
		return
	}

}

// =======================
// testCase1
// result：正常
// =======================
func testCase1(url, reqJSON string) error {

	c, err := contextCreator(echo.POST, url, reqJSON)
	if err != nil {
		return err
	}

	u, err := InsertUser(c)
	if err != nil {
		return err
	}

	if u.Name != "Taro" {
		return errors.New("InsertUser Validator Error. Name == Taro")
	}

	return nil

}

// =======================
// testCase2
// result：異常
// name：nil
// =======================
func testCase2(url, reqJSON string) error {

	c, err := contextCreator(echo.POST, url, reqJSON)
	if err != nil {
		return err
	}

	_, err = InsertUser(c)
	if err == nil {
		return errors.New("InsertUser Validator Error. Name is Nil")
	}

	return nil

}

// =======================
// testCase3
// result：異常
// age：nil
// =======================
func testCase3(url, reqJSON string) error {

	c, err := contextCreator(echo.POST, url, reqJSON)
	if err != nil {
		return err
	}

	_, err = InsertUser(c)
	if err == nil {
		return errors.New("InsertUser Validator Error. Age is Nil")
	}

	return nil

}

// =======================
// testCase4
// result：異常
// device_code：なし
// =======================
func testCase4(url, reqJSON string) error {

	c, err := contextCreator(echo.POST, url, reqJSON)
	if err != nil {
		return err
	}

	_, err = InsertUser(c)
	if err == nil {
		return errors.New("InsertUser Validator Error.")
	}

	return nil
}

func contextCreator(method, url, body string) (echo.Context, error) {

	e := echo.New()
	req, err := http.NewRequest(method, url, strings.NewReader(body))
	if err != nil {
		return nil, err
	}

	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(standard.NewRequest(req, e.Logger()), standard.NewResponse(rec, e.Logger()))

	return c, err

}
