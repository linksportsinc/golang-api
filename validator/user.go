package validator

import (
	"github.com/labstack/echo"
	"github.com/yuri-swift/golang-api/model"
	"gopkg.in/go-playground/validator.v9"
)

func InsertUser(c echo.Context) (*model.User, error) {

	var validate *validator.Validate
	validate = validator.New()
	validate.RegisterStructValidation(UserValidation, model.User{})

	u := &model.User{}
	c.Bind(u)

	return u, validate.Struct(u)
}

func UserValidation(sl validator.StructLevel) {

	input := sl.Current().Interface().(model.User)

	// Name
	if input.Name == "" {
		sl.ReportError(input, "Name", "Not Null", "", "")
		return
	}

	// Age
	if input.Age == 0 {
		sl.ReportError(input, "Age", "Not Null", "", "")
		return
	}
}
