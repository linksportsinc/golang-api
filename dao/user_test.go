package dao

import (
	"fmt"
	"strconv"
	"testing"

	"github.com/yuri-swift/golang-api/db"
	"github.com/yuri-swift/golang-api/model"
)

func TestNewUser(t *testing.T) {
	d := NewUser(db.GetSession())
	if d == nil {
		t.Error("DB. Session Create Error（users）")
		return
	}
}

func TestCreate(t *testing.T) {

	user := &model.User{
		Name: "Taro",
		Age:  1,
	}

	sess := db.GetSession()
	userImpl := UserImpl{db: sess}

	i, err := userImpl.Create(user)
	if err != nil {
		t.Error(err)
		return
	}

	defer userImpl.db.DeleteFrom("users").
		Where("id = ?", i).
		Exec()

	actualUser := model.User{}
	userImpl.db.SelectBySql("SELECT * FROM users WHERE id = ?", i).
		Load(&actualUser)
	fmt.Printf("%v", actualUser)

	if &actualUser == nil {
		t.Error("Create SQL Error. Cannot create User Record.")
		return
	}

	if user.Name != actualUser.Name {
		t.Error(
			"Create SQL Error by Name. Expect: " +
				user.Name +
				", Actual: " +
				actualUser.Name +
				", id: " +
				strconv.Itoa(int(i)),
		)
		return
	}

	if user.Age != actualUser.Age {
		t.Error(
			"Create SQL Error by Age. Expect: " +
				strconv.Itoa(user.Age) +
				", Actual: " +
				strconv.Itoa(actualUser.Age) +
				", id: " +
				strconv.Itoa(int(i)),
		)
		return
	}
}
