package dao

import (
	"github.com/gocraft/dbr"
	"github.com/yuri-swift/golang-api/model"
)

type User interface {
	SelectUsers() *[]model.User
	Create(u *model.User) (int64, error)
}

type UserImpl struct {
	db *dbr.Session
}

func NewUser(db *dbr.Session) User {
	return &UserImpl{db: db}
}

func (g UserImpl) SelectUsers() *[]model.User {

	var users []model.User
	g.db.Select("*").
		From("users").
		OrderBy("id").
		Load(&users)

	return &users

}

func (u UserImpl) Create(user *model.User) (int64, error) {

	res, _ := u.db.InsertInto("users").
		Columns("name", "age").
		Values(user.Name, user.Name).
		Exec()

	return res.LastInsertId()

}
