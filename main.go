package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
	echoMw "github.com/labstack/echo/middleware"
	"github.com/valyala/fasthttp"
	"github.com/yuri-swift/golang-api/api"

	_ "github.com/mattes/migrate/source/file"
)

func main() {

	e := echo.New()

	// Middleware
	e.Use(echoMw.Logger())
	e.Use(echoMw.Gzip())
	e.Use(echoMw.CORSWithConfig(echoMw.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAcceptEncoding},
	}))
	e.SetHTTPErrorHandler(JSONHTTPErrorHandler)

	// Routes

	g := e.Group("/api")
	{
		g.GET("/users/", api.SelectUsers)
		g.POST("/users/create/", api.InsertUser)
		g.POST("/users/update/", api.UpdateUser)
		g.POST("/users/delete/", api.DeleteUser)
	}

	// Start server
	e.Run(standard.New(":8000"))
}

func JSONHTTPErrorHandler(err error, c echo.Context) {
	code := fasthttp.StatusInternalServerError
	msg := "Internal Server Error"
	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
		msg = he.Message
	}
	if !c.Response().Committed() {
		c.JSON(code, map[string]interface{}{
			"statusCode": code,
			"message":    msg,
		})
	}
}
