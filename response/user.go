package response

import (
	"strconv"

	"github.com/yuri-swift/golang-api/model"
)

func SelectUsers(users *[]model.User) *map[string]interface{} {

	var respUsers []map[string]string
	for _, v := range *users {
		user := map[string]string{
			"id":   strconv.Itoa(v.Id),
			"name": v.Name,
			"age":  strconv.Itoa(v.Age),
		}
		respUsers = append(respUsers, user)
	}

	resp := map[string]interface{}{
		"user": respUsers,
	}
	return &resp
}

func InsertUser(id int64) *map[string]string {
	User := map[string]string{
		"id": strconv.Itoa(int(id)),
	}
	return &User
}
