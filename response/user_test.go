package response

import (
	"errors"
	"strconv"
	"testing"
)

func TestInsertUser(t *testing.T) {

	err := testInsertUserCase1(int64(1))
	if err != nil {
		t.Error(err)
		return
	}
}

// =======================
// testCase1
// result：正常
// device_code：1
// =======================
func testInsertUserCase1(id int64) error {

	u := *InsertUser(id)

	actualId, ok := u["id"]
	if !ok || actualId != strconv.Itoa(int(id)) {
		return errors.New("PostUser Response Error. DeviceCode == 1")
	}

	return nil
}
