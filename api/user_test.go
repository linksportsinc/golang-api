package api

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"

	"github.com/bitly/go-simplejson"
	"github.com/labstack/echo"
	"github.com/valyala/fasthttp"
)

var (
	reqJsonCase1 = `{"device_code": "1"}`
	reqJsonCase2 = `{"device_code": "0"}`
)

func TestInsertUser(t *testing.T) {

	url := "http://localhost:8000/api/users/"

	err := testInsertUserCase1(url, reqJsonCase1)
	if err != nil {
		t.Error(err)
		return
	}

	err = testInsertUserCase2(url, reqJsonCase2)
	if err != nil {
		t.Error(err)
		return
	}

}

// =======================
// testCase1
// result：正常
// =======================
func testInsertUserCase1(url, reqJson string) error {

	c, rec, err := contextCreator(echo.POST, url, reqJson)
	if err != nil {
		return err
	}

	err = InsertUser(c)
	if err != nil {
		return err
	}

	expectCode := fasthttp.StatusCreated
	if expectCode != rec.Code {
		return errors.New("Response Code Error. Expect: " +
			strconv.Itoa(expectCode) +
			", Actual:" +
			strconv.Itoa(rec.Code),
		)
	}

	json, err := simplejson.NewJson(rec.Body.Bytes())
	i := json.Get("id").MustString()

	if i == "" {
		return errors.New("response error")
	}

	return nil

}

// =======================
// testCase2
// result：異常（StatusBadRequest）
// =======================
func testInsertUserCase2(url, reqJson string) error {

	c, rec, err := contextCreator(echo.POST, url, reqJson)
	if err != nil {
		return err
	}

	err = InsertUser(c)
	if err != nil {
		return err
	}

	expectCode := fasthttp.StatusBadRequest
	if expectCode != rec.Code {
		return errors.New("Response Code Error. Expect: " +
			strconv.Itoa(expectCode) +
			", Actual:" +
			strconv.Itoa(rec.Code),
		)
	}

	return nil

}

func contextCreator(method, url, body string) (echo.Context, *httptest.ResponseRecorder, error) {

	e := echo.New()
	req, err := http.NewRequest(method, url, strings.NewReader(body))
	if err != nil {
		return nil, nil, err
	}

	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(standard.NewRequest(req, e.Logger()), standard.NewResponse(rec, e.Logger()))

	return c, rec, err

}
