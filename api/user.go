package api

import (
	"github.com/labstack/echo"
	"github.com/valyala/fasthttp"
	"github.com/yuri-swift/golang-api/dao"
	"github.com/yuri-swift/golang-api/db"
	"github.com/yuri-swift/golang-api/response"
	"github.com/yuri-swift/golang-api/validator"
)

func SelectUsers(c echo.Context) error {
	d := dao.NewUser(db.GetSession())
	users := d.SelectUsers()

	return c.JSON(fasthttp.StatusOK, response.SelectUsers(users))
}

func InsertUser(c echo.Context) error {

	u, err := validator.InsertUser(c)
	if err != nil {
		return c.JSON(fasthttp.StatusBadRequest, nil)
	}

	d := dao.NewUser(db.GetSession())
	id, _ := d.Create(u)

	return c.JSON(fasthttp.StatusCreated, response.InsertUser(id))
}

func UpdateUser(c echo.Context) error {
	// TODO:
	return c.JSON(fasthttp.StatusCreated, nil)
}

func DeleteUser(c echo.Context) error {
	// TODO:
	return c.JSON(fasthttp.StatusCreated, nil)
}
